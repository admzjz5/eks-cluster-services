locals {
  calico_apply_triggers = module.aws_k8s_cni_calico_manifest_apply.apply_triggers
}

#############################################################################################
# Reference: https://docs.aws.amazon.com/eks/latest/userguide/calico.html
#############################################################################################

# Apply the manifest to the cluster
#    - tries delete on destroy but does not produce any errors
module "aws_k8s_cni_calico_manifest_apply" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply?ref=v1.0.11"

  enabled = var.enable_aws_k8s_cni_calico_plugin

  enable_delete_on_destroy = true # un-install on destroy
  silence_delete_errors    = true # don't break terraform destroy if k8s in not responding well to delete

  silence_apply_errors = false # break terraform apply is apply fails

  manifest   = module.aws_k8s_cni_calico_manifest_download.content
  kubeconfig = local.kubeconfig_file
}

# Download the manifest and save it to file
module "aws_k8s_cni_calico_manifest_download" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/utils/download-content?ref=v1.0.0"

  enabled = var.enable_aws_k8s_cni_calico_plugin

  source_url = var.aws_k8s_cni_calico_manifest_url

  save_to_file     = true
  destination_file = "${var.config_output_path}/${var.aws_k8s_cni_calico_manifest_file}"
}
