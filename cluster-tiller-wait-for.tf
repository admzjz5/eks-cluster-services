# All resources of the helm provider need a running tiller pod,
# therefore we need to block terraform execution of any helm resource
# untill tiller is ready, or fail the execution.
#
# all helm resources need to depend on this one
resource "null_resource" "wait_for_tiller" {
  provisioner "local-exec" {
    command = join(" ", [
      "kubectl wait --for=condition=Ready --timeout=${var.cluster_tiller_wait_for_timeout}",
      "$(kubectl get pod -l app=helm -l name=tiller -n ${local.tiller_service_namespace} --kubeconfig ${local.kubeconfig_file}  -oname)",
      "-n ${local.tiller_service_namespace} --kubeconfig ${local.kubeconfig_file}"
    ])
  }

  triggers = {
    always = uuid() // always trigger, otehrwise it only triggers once and when any input var changes
  }

  depends_on = [
    data.null_data_source.external_dependencies,
    kubernetes_deployment.tiller_deploy,
  ]
}
