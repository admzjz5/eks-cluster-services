locals {
  eks_iam_service_account_annotation = "eks.amazonaws.com/role-arn"

  kubeconfig_file = local_file.kubeconfig.filename
}
