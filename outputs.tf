output "calico_triggers" {
  value = sha1("${join(",", keys(local.calico_apply_triggers))}-${join(",", values(local.calico_apply_triggers))}")
}
