locals {
  tiller_deploy_name  = "tiller-deploy"
  tiller_service_name = "tiller-deploy"

  tiller_service_namespace = "kube-system"

  need_helm_release = var.enable_autoscaler || var.enable_spot_instance_handler || var.enable_istio

  # other module services might need tiller anywaw
  enable_cluster_tiller = var.enable_cluster_tiller || local.need_helm_release
}

resource "kubernetes_service" "tiller_deploy" {
  count = local.enable_cluster_tiller ? 1 : 0

  metadata {
    name      = local.tiller_service_name
    namespace = local.tiller_service_namespace

    labels = {
      app  = "helm"
      name = "tiller"
    }
  }

  spec {
    selector = {
      app  = "helm"
      name = "tiller"
    }

    port {
      name        = "tiller"
      port        = 44134
      target_port = "tiller"
    }
  }

  depends_on = [
    data.null_data_source.external_dependencies,
  ]
}

resource "kubernetes_deployment" "tiller_deploy" {
  count = local.enable_cluster_tiller ? 1 : 0

  metadata {
    name      = local.tiller_deploy_name
    namespace = local.tiller_service_namespace

    labels = {
      app  = "helm"
      name = "tiller"
    }
  }

  spec {
    min_ready_seconds      = 15
    replicas               = 1
    revision_history_limit = 10

    strategy {
      type = "Recreate"
    }

    selector {
      match_labels = {
        app  = "helm"
        name = "tiller"
      }
    }

    template {
      metadata {
        namespace = ""

        labels = {
          app  = "helm"
          name = "tiller"
        }
      }

      spec {
        service_account_name = var.cluster_tiller_service_account_name
        restart_policy       = "Always"

        termination_grace_period_seconds = 30

        automount_service_account_token = "true"

        priority_class_name = var.cluster_tiller_priority_class_name

        container {
          image             = "gcr.io/kubernetes-helm/tiller:${var.cluster_tiller_container_image_tag}"
          name              = "tiller"
          image_pull_policy = "IfNotPresent"

          port {
            container_port = 44134
            name           = "tiller"
            protocol       = "TCP"
          }

          port {
            container_port = 44135
            name           = "http"
            protocol       = "TCP"
          }

          env {
            name  = "TILLER_NAMESPACE"
            value = local.tiller_service_namespace
          }

          env {
            name  = "TILLER_HISTORY_MAX"
            value = "0"
          }

          liveness_probe {
            http_get {
              path   = "/liveness"
              port   = 44135
              scheme = "HTTP"
            }

            period_seconds        = 10
            initial_delay_seconds = 1
            success_threshold     = 1
            failure_threshold     = 3
            timeout_seconds       = 1
          }

          readiness_probe {
            http_get {
              path   = "/readiness"
              port   = 44135
              scheme = "HTTP"
            }

            period_seconds        = 10
            initial_delay_seconds = 1
            success_threshold     = 2
            failure_threshold     = 3
            timeout_seconds       = 1
          }
        }
      }
    }
  }

  depends_on = [
    data.null_data_source.external_dependencies,
    module.aws_k8s_cni_calico_manifest_apply,
    kubernetes_cluster_role_binding.tiller_deploy
  ]
}

resource "kubernetes_service_account" "tiller_deploy" {
  count = local.enable_cluster_tiller ? 1 : 0

  automount_service_account_token = true

  metadata {
    name      = var.cluster_tiller_service_account_name
    namespace = local.tiller_service_namespace
  }

  depends_on = [data.null_data_source.external_dependencies]
}

resource "kubernetes_cluster_role_binding" "tiller_deploy" {
  count = local.enable_cluster_tiller ? 1 : 0

  metadata {
    name = local.tiller_deploy_name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = var.cluster_tiller_service_account_name
    namespace = local.tiller_service_namespace
  }

  depends_on = [
    data.null_data_source.external_dependencies,
    kubernetes_service_account.tiller_deploy,
  ]
}
