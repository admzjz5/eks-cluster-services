locals {
  istio_init_name = "istio-init"

  istio_init_helm_values = var.enable_istio ? templatefile("${path.module}/files/templates/${local.istio_init_name}-values.yaml.tpl", {
    container_image_tag = var.istio_container_image_tag
  }) : ""
}

resource "helm_release" "istio_init" {
  count = var.enable_istio ? 1 : 0

  name      = local.istio_init_name
  chart     = "${local.istio_helm_repository_name}/${local.istio_init_name}"
  namespace = local.istio_namespace
  version   = var.istio_helm_chart_version

  timeout = 1200

  values = [local.istio_init_helm_values]

  depends_on = [
    data.null_data_source.external_dependencies,
    null_resource.wait_for_tiller,
  ]
}
