locals {
  istio_name      = "istio"
  istio_namespace = "istio-system"

  istio_helm_values = var.enable_istio ? templatefile("${path.module}/files/templates/${local.istio_name}-values.yaml.tpl", {
    container_image_tag       = var.istio_container_image_tag
    mtls_enabled              = var.istio_mtls_enabled
    mtls_auto                 = var.istio_mtls_auto
    config_validation_enabled = var.istio_config_validation_enabled
    enable_sidecar_injector   = var.istio_sidecar_injector_enabled
    https_enabled             = var.istio_gateway_https_enabled
  }) : ""

  istio_helm_repository_name = join("", data.helm_repository.istio.*.name)
}

data "helm_repository" "istio" {
  count = var.enable_istio ? 1 : 0

  name = "istio.io"
  url  = "https://storage.googleapis.com/istio-release/releases/${var.istio_helm_chart_version}/charts/"
}

resource "helm_release" "istio" {
  count = var.enable_istio ? 1 : 0

  name      = local.istio_name
  chart     = "${local.istio_helm_repository_name}/${local.istio_name}"
  namespace = local.istio_namespace
  version   = var.istio_helm_chart_version

  timeout = 1200

  values = concat([local.istio_helm_values], var.istio_helm_values)

  depends_on = [
    data.null_data_source.external_dependencies,
    null_resource.wait_for_tiller,
    helm_release.istio_init,
    helm_release.istio_cni,
  ]
}
