# Hack to ensure ordering of resource creation, this is
# "do not start creating any reosurces before these dependencies have produced their putputs"
#
# All resources in this module need to depend on this data source
data "null_data_source" "external_dependencies" {
  inputs = var.external_dependencies
}

# Since we heavily depend on the content of this file
# it's better to make our own copy that only gets deleted when the module is destroyed
resource "local_file" "kubeconfig" {
  filename = "${var.config_output_path}/.module.open-source-devex.kubernetes.eks-cluster-services/kubeconfig"
  content  = var.kubeconfig

  lifecycle {
    create_before_destroy = true # when re-creating, create before destroy
  }
}
