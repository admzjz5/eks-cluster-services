logLevel: info

repair:
  deletePods: true

excludeNamespaces:
  - istio-system
  - kube-system
