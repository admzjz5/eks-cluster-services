global:
  # Default tag for Istio images.
  tag: ${container_image_tag}-distroless

  controlPlaneSecurityEnabled: true

  configValidation: ${config_validation_enabled}

  k8sIngress:
    enabled: true
    enableHttps: ${https_enabled}

  proxy:
    accessLogFile: "/dev/stdout"

  mtls:
    # Default setting for service-to-service mtls. Can be set explicitly using
    # destination rules or service annotations.
    enabled: ${mtls_enabled}

    # If set to true, and a given service does not have a corresponding DestinationRule configured,
    # or its DestinationRule does not have TLSSettings specified, Istio configures client side
    # TLS configuration automatically, based on the server side mTLS authentication policy and the
    # availability of sidecars.
    auto: ${mtls_auto}

  sds:
    enabled: true
    udsPath: "unix:/var/run/sds/uds_path"
    token:
      aud: "istio-ca"

nodeagent:
  enabled: true
  image: node-agent-k8s
  env:
    CA_PROVIDER: "Citadel"
    CA_ADDR: "istio-citadel:8060"
    VALID_TOKEN: true

istio_cni:
  enabled: true

prometheus:
  enabled: false

certmanager:
  enabled: false

gateways:
  enabled: true
  istio-ingressgateway:
    enabled: true
    sds:
      # If true, ingress gateway fetches credentials from SDS server to handle TLS connections.
      enabled: false
    type: NodePort  # AWS lb will be the actual ingress and direct traffic to the istio ingress gateway port
    serviceAnnotations:
      service.beta.kubernetes.io/aws-load-balancer-type: "alb"

sidecarInjectorWebhook:
  enabled: ${enable_sidecar_injector}
  rewriteAppHTTPProbe: true
