nameOverride: ${name_override}

image:
  repository: eu.gcr.io/k8s-artifacts-prod/autoscaling/cluster-autoscaler
  tag: ${image_tag}

cloudProvider: aws
awsRegion: "${region}"

autoDiscovery:
  clusterName: "${cluster_name}"

rbac:
  create: true
  pspEnabled: false
  serviceAccount:
    create: true
  serviceAccountAnnotations:
    eks.amazonaws.com/role-arn: "${service_account_iam_role_annotation}"
