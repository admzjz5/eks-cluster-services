locals {
  istio_cni_name      = "istio-cni"
  istio_cni_namespace = "kube-system"

  istio_cni_helm_values = var.enable_istio ? templatefile("${path.module}/files/templates/${local.istio_cni_name}-values.yaml.tpl", {}) : ""
}

resource "helm_release" "istio_cni" {
  count = var.enable_istio ? 1 : 0

  name      = local.istio_cni_name
  chart     = "${local.istio_helm_repository_name}/${local.istio_cni_name}"
  namespace = local.istio_cni_namespace
  version   = var.istio_helm_chart_version

  timeout = 1200

  values = [local.istio_cni_helm_values]

  depends_on = [
    data.null_data_source.external_dependencies,
    null_resource.wait_for_tiller,
    helm_release.istio_init,
  ]
}
